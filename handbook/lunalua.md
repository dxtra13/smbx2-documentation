# LunaLua

## Automatically accessible features

These features can be used without the need to load any
external libraries. Some are more complicated to use than others, but
all are helpful to keep in mind.

### Added functionality to basegame classes

Certain basegame namespaces have been augmented with
new functionality. Below is a list of all features that have been
added.

```lua
Section.getActiveIndices() --Returns all sections in which players currently are
Section.getWeatherEffect(id) --Gets a particle emitter matching one of the weather effect constants
                                                       (WEATHER_RAIN, WEATHER_SNOW, WEATHER_FOG, WEATHER_SANDSTORM,
                                                       WEATHER_CINDERS, WEATHER_WISPS)
Section:drawScreenEffect(id, camera) --Draws a screen effect using one of the screen effect constants
                                                                       (SEFFECT_WAVY, SEFFECT_LAVA, SEFFECT_CAUSTICS, 
                                                                       SEFFECT_UNDERWATER, SEFFECT_MIST, SEFFECT_SEPIA, 
                                                                       SEFFECT_GRAYSCALE, SEFFECT_INVERTED, SEFFECT_GAMEBOY,
                                                                       SEFFECT_DITHERED_GAMEBOY)

Player:render{args} --Renders the player. The function has a lot of customisation options, documented here: https://pastebin.com/dfGz4ZDa
Player.getNearest(x,y) --Returns closest player [[LEVEL ONLY]]
Player:transform(id, shouldSpawnEffect?) --Transforms the player into the character of the ID provided, with
                                                                               a smoke cloud effect if desired.
Player.setCostume(id, costumeName, volatile?) --Sets the costume for the character of the ID provided to 
                                                                                        the costume with the corresponding name. CostumeName
                                                                                        has to match the folder name of the costume in the 
                                                                                        costumes folder. For example, “SMW-Mario”. If volatile is 
                                                                                        set to true, the costume state isn’t saved.
Player.getCostume(id) --Returns the name of the costume currently equipped by the character with the 
                                           given ID

Text.getSize(string text, int fonttype) --Returns width, height of the string specified if rendered with 
                                                                     Text.print with the corresponding font type


Misc.episodePath() --Gets total path to the episode
Misc.inEditor() --Are we playing in the editor?
Misc.isPaused() --Is the game paused either by vanilla or lua?
Misc.dialog(a) --Shows a dialog box, displaying a. Can accept multiple arguments. Intended for debugging.
Misc.givePoints(index, position, supressSound?) --Provides score or lives relative to the internal score 
                                                                                         indexing, renders the effect at the given position and plays 
                                                                                         a sound if desired
Misc.setBPM(b) --Sets the level’s BPM, used for blinking blocks and timed spike blocks
Misc.getBPM() --Gets the level’s BPM, used for blinking blocks and timed spike blocks
Misc.setBeatTime(s) --Sets the time between beats, used for blinking blocks and timed spike blocks
                                        This also adjusts the level’s BPM
Misc.getBeatTime() --Gets the time between beats, used for blinking blocks and timed spike blocks
Misc.setBeatSignature(s) --Sets the number of beats in a bar, used for blinking blocks and timed spike blocks
Misc.getBeatSignature() --Gets the number of beats in a bar, used for blinking blocks and timed spike blocks
Misc.beatUsesMusicClock --If set to true, the beat will be counted to keep sync with the music
Misc.getBeatClock() --Gets the current beat tick counter, used for blinking blocks and timed spike blocks

Level.load(filename, episodename, warpindex) --Loads the specified level [[LEVEL ONLY]]
Level.folderPath() --Returns the path to the current level folder [[LEVEL ONLY]]

Layer.isPaused() --Check if layers are currently paused [[LEVEL ONLY]]

--Transforms an NPC. Centered defaults to true and binds to bottom/center for NPCs with gravity to prevent clipping. changeSpawn defaults to false, and if changed will change the spawn ID of the NPC to newID [[LEVEL ONLY]]
NPC:transform(newID, centered, changeSpawn)


Graphics.drawBox{args} --Helper function for glDraw which takes x,y,width,height rather than a vertex table
Graphics.drawScreen{args} --Helper function for glDraw which draws over the entire screen
Graphics.drawLine{args} --Helper function for glDraw which draws a line between x1,y1 and x2,y2
Graphics.drawCircle{args} --Helper function for glDraw which draws a circle with x,y and radius

table.ifindlast(t, value) --Finds last instance of value in t
table.findlast(t, value)  --Works for tables with gaps in their indexing
table.ifind(t, value) --Finds first instance of value in t
table.find(t, value)  --Works for tables with gaps in their indexing
table.ifindall(t, value) --Finds all instances of value in t
table.findall(t, value)  --Works for tables with gaps in their indexing
table.icontains(t, value) --Returns whether t has value
table.contains(t, value)  --Works for tables with gaps in their indexing
table.iclone(t) --Performs a shallow clone of t
table.clone(t)  --Works for tables with gaps in their indexing
table.ideepclone(t) --Performs a deep clone. Performance-intensive!
table.deepclone(t)  --Works for tables with gaps in their indexing
table.ishuffle(t) --Shuffles a table with consecutive numeric indices
table.map(t) --Returns a lookup map of t
table.unmap(t) --Reserve operation of table.map(t)
table.join(...) --Arguments are joined in reverse order, returns result
table.append(...) --Values of arguments are appended in order, returns result
table.reverse(t) --Reverses a table
table.flatten(t) --Flattens an array of vectors, matrices, colors, or similar contents, turning it to a numerically 
                              ordered table without subtables. Useful for uniforms and attributes in shaders, which 
                              require flattened arrays as their input.

string.trim(s) --Trims trailing and leading whitespace
string.split(s, pattern, exclude?, plain?)  --Split a string on a pattern into a table of strings
string.compare(left, right) --Compares two strings by lexical ordering and length, returning -1 or 1 depending
                                                   on whether the left or right string is lexically or literally smaller. Returns 0 when
                                                   the strings are identical.

math.lerp(a,b,t) --Interpolates from a to b given time value t (between 0 and 1)
math.anglelerp(a,b,t) --Interpolates between 0 and 360, wrapping around the ends
math.invlerp(a,b,v) --Performs an inverse lerp
math.clamp(a,min,max) --Clamps a between the limits
math.sign(a) -- Returns the signum of a
```

-----

### Advanced Sprite Drawing

The Sprite namespace makes complicated draw calls easy. While for
simple draw calls the
[Graphics.drawImage](https://wohlsoft.ru/pgewiki/LunaLua_global_functions%23Graphics_functions) family
of functions will suffice, you will be unable to handle scaling,
rotation and tints without an alternative method.

The Sprite namespace has extended documentation in its
source file which can be found in
“SMBX\\data\\scripts\\base\\Sprite.lua”. Its general workflow can be
split into three parts though:

1.  Creating a Sprite object
2.  Manipulating a Sprite object
3.  Drawing the Sprite object

Here is an example:

```lua
--Creation
local image = Sprite{x = 0, y = 0, frames = 10, texture = someImage}

function onDraw()
   image:rotate(1)
   image:draw{frame = 1}
end
```

### Collision detection

Using functions of the Colliders namespace, collision
detection can be done between Blocks, NPCs, Players, and user-defined
collider objects. You can find documentation for the library on the
following page, though it is somewhat outdated. These days, Colliders is
automatically active, and you do not need to explicitly load the library
in order to use it:

[http://wohlsoft.ru/pgewiki/Colliders.lua](http://wohlsoft.ru/pgewiki/Colliders.lua)

Example:

```lua
--Creation
local box = Colliders.Box(-200000, -200600, 800, 600)

function onTick()
    --Collision Check
    if not Colliders.collide(player, box) then
        Text.print(“The player has left the first screen”, 100, 100)
    end
end
```

If you find yourself needing to check collisions
between lots of objects, for example, to see if a collider is touching
an NPC, you should consider using Colliders.getColliding. This will give
you a list of objects that satisfy your conditions. You can also ask for
condition pairs (for example, if you need to find which NPCs of a given
ID are touching certain blocks of a different ID), which will give you a
list of colliding pairs. You can optionally supply a “filter” function,
that will test every object in your provided list. You can use this if
you need to fulfil certain conditions such as memory locations.

Example:

```lua
--Creation
local box = Colliders.Box(-200000, -200600, 800, 600)

function onTick()
    --Collision Check
    local list = Colliders.getColliding{
                                         a = box, 
                                         b = NPC.HITTABLE, 
                                         btype = Colliders.NPC
                                        }
    for k,v in ipairs(list) do
        Text.print(“Touching NPC at ”..v.x..“, ”..v.y, 100, k*20)
    end
end
```

### Colors

This version of SMBX2 introduces a way to define and
use colors. Using the Color namespace you can access existing colors,
define your own, perform math on these colours and lerp between them.
Predefined color constants and their corresponding hexadecimal values
can be found below:

```lua
Color.white       --0xFFFFFFFF
Color.black       --0x000000FF
Color.red         --0xFF0000FF
Color.green       --0x00FF00FF
Color.blue        --0x0000FFFF
Color.alphawhite  --0xFFFFFF00
Color.alphablack  --0x00000000
Color.transparent --0x00000000
Color.grey        --0x808080FF
Color.gray        --0x808080FF
Color.cyan        --0x00FFFFFF
Color.magenta     --0xFF00FFFF
Color.yellow      --0xFFFF00FF
Color.pink        --0xFF73ABFF
Color.canary      --0xFFF266FF
Color.purple      --0xAB66ABFF
Color.orange      --0xFF8C54FF
Color.teal        --0x00AB99FF
Color.maroon      --0x730000FF
Color.brown       --0x804D00FF
Color.lightgrey   --0xBFBFBFFF
Color.lightgray   --0xBFBFBFFF
Color.lightblue   --0x33CCFFFF
Color.lightgreen  --0x80CC99FF
Color.lightbrown  --0xBF9966FF
Color.lightred    --0xFF8080FF
Color.darkgrey    --0x404040FF
Color.darkgray    --0x404040FF
Color.darkblue    --0x003373FF
Color.darkgreen   --0x005926FF
Color.darkbrown   --0x4D4040FF
Color.darkred     --0x800000FF
```

Usage examples:

```lua
--Adjustment of color alpha through concatenation:
local alphared = Color.red .. 0

--Definitions of a custom color
local myColor = Color(1,1,1)
local myColor = Color.fromHex(0x112233FF)
local myColor = Color.fromHexRGB(0x112233)

--While lerp lerps between RGB values
local newColor = math.lerp(Color.red, Color.blue, timer)
--lerpHSV uses the HSV values for its lerp
local newColor = Color.lerpHSV(Color.red, Color.blue, timer)
```

Extended documentation on the Color class can be found
in “SMBX\\data\\scripts\\base\\engine\\color.lua”

### Liquid Class

Liquids such as Water and (don’t ask) Quicksand are now
accessible from code. While in most situations simply checking for an
entity’s “underwater” state can be enough, there are some scenarios
where knowing and manipulating the exact bounds of liquid boxes can be
useful. The class is equipped with the same functions other base object
classes have:

```lua
-- Methods:
Liquid.get() -- returns all liquids in the level
Liquid.getIntersecting(x1, y1, x2, y2) -- returns all liquids in area
Liquid.count() -- returns number of liquids in the level

-- Fields:
myLiquid.idx -- index in Liquid array
myLiquid.isValid -- validity check
myLiquid.layer -- Layer object
myLiquid.layerName -- name of layer object
myLiquid.isHidden -- visibility flag
myLiquid.isQuicksand -- if true, this liquid is quicksand
myLiquid.x
myLiquid.y
myLiquid.width
myLiquid.height
myLiquid.speedX
myLiquid.speedY
```

### Save Data

There are two exposed tables for save data storage
accessible at all times:

  - SaveData - Saves data across sessions
  - GameData - Saves data for this session only

By writing to these tables, you can store information
that persists past a level reload. This can be used for various effects,
like checking whether or not to repeat a cutscene the player has already
seen, or saving one-time collectibles. You can simply define new
variables to keep track of by storing them as part of these
tables:

```lua
SaveData.hasBeatenLevel = false
GameData.skipCutscene = false
```

SaveData and GameData’s tables are global
for an episode. If you are working on a level
that is for a contest or collaboration, and the event in question
doesn’t specify its own save data handling, please handle your save
data as follows in order to avoid any
conflicts:![](images/image3.png)

```lua
--Initialisation of your own save data
SaveData[Level.filename()] = SaveData[Level.filename()] or {}
GameData[Level.filename()] = GameData[Level.filename()] or {}

local saveData = SaveData[Level.filename()]
local gameData = GameData[Level.filename()]
```

You can now use the saveData and gameData variables
just like you would use SaveData and GameData, with the added benefit of
avoiding any possible conflicts with save data from other levels in the
collab or contest.

If you really need to, SaveData and GameData both have
special functions you can use for some extra functionality, though
usually these won’t be necessary:

```lua
SaveData.flush() --Forceibly write your SaveData to the save file. 
                   Normally, this will be automatically done when the
                   Game is saved

SaveData.clear() --Wipes everything from SaveData. Won’t be saved until
                   the game is saved (or SaveData.flush() is called).
GameData.clear() --Wipes everything from GameData.
```

### Sound and Music

You can use the following function to play
music:

```lua
Audio.MusicChange((number) section, (string) filename or (number) musicID)
```

To stop music entirely, you can set the music to ID 0 -
silence.

You can also use the Audio namespace’s functions to
control music, though using MusicChange is usually simpler:

[https:--wohlsoft.ru/pgewiki/LunaLua\_global\_Sound\_and\_Music\_functions](https://wohlsoft.ru/pgewiki/LunaLua_global_Sound_and_Music_functions)

Sound effects have been streamlined. You can use the
versatile SFX.play function to cover your needs for sound
effects:

```lua
--You can use the set overload:
SFX.play((number) internalindex or (string) filename, volume, loops, delay)
--or named arguments:
SFX.play{args}

--Available arguments:
sound              --REQUIRED: The sound file path
loops              --The number of loops for this sound to play for. 0 to loop forever. Defaults to 1.
volume             --The volume of this audio clip, between 0 and 1. Defaults to 1.
pan                --The left/right panning of this audio clip, between -1 and 1. Defaults to 0.
tags               --List of tags for this sound clip. Allows volume to be adjusted for every sound with a given tag.
tag                --Single tag for this sound clip. Allows volume to be adjusted for every sound with a given tag.
delay              --The number of ticks before the same sound can be played again. Defaults to 4.
```

Examples:

```lua
SFX.play(4)
SFX.play(“mySound.ogg”, 0.5)
SFX.play(37, 1, 3, 18)
SFX.play{sound=“mySound.ogg”}
```

In addition to SFX.play, you are able to use
SFX.create, which creates a physical audio source in the scene and is
useful for area-specific sound effects like the rushing of a
waterfall.

```lua
SFX.create{args}

--Available arguments:
--x,y                       --REQUIRED: Position of the centre of the audio source.
--falloffRadius             --REQUIRED: Distance the sound travels from the source before it is silent.
--sound                     --The sound file path
--falloffType               --The falloff function to use. Supports FALLOFF_NONE, FALLOFF_LINEAR and 
                              FALLOFF_SQUARE. Can also use a custom function of the form 
                              'falloff(squaredFalloff, squaredDistance)'. Defaults to FALLOFF_SQUARE.
--type                      --Shape of the audio source (to emit at max volume). Supports SOURCE_POINT, 
                              SOURCE_CIRCLE, SOURCE_BOX and SOURCE_LINE. Defaults to SOURCE_POINT.
--play                      --Should the sound play immediately? Defaults to true.
--loops                     --The number of loops for this sound to play for. 0 to loop forever. Defaults to 0.
--volume                    --The volume of this audio source, between 0 and 1. Defaults to 1.
--parent                    --Object to attach the source to. Defaults to nil.
--tags                      --List of tags for this sound source. Allows volume to be adjusted for every sound with a 
                              given tag.
--tag                       --Single tag for this sound source. Allows volume to be adjusted for every sound with a 
                              given tag.

----SOURCE_CIRCLE only
--sourceRadius              --REQUIRED: The radius of the audio source.

----SOURCE_BOX only
--sourceWidth/sourceHeight --REQUIRED: The dimensions of the audio source.

----SOURCE_LINE only
--sourceVector             --REQUIRED: The vector describing the line. Line will span from {x,y} to 
                                       {x,y}+sourceVector.
```

### Vector Math

Using the vector namespace, you are able to use vector mathematics
in two, three and four dimensions. Its usage has changed minimally since
Beta 3 and the library is now automatically loaded into the vector
namespace. Note\! The namespace
“vector” is not capitalised, unlike the rest of the namespaces in this
section\!

[Vector
documentation](http://engine.wohlnet.ru/pgewiki/VectR.lua)

```lua
--To create an n-dimensional vector, you can use the following constructor. Depending on the number of arguments you provide, you will get a vector in a different dimension.
local myVector = vector(x,y,z,w)

--Examples
local myVector = vector(12, 3, 4, 8) --4D Vector
local myVector = vector(2, 8, 6) --3D Vector
local myVector = vector(3, 15) --2D Vector
local myVector = vector(5) --Shorthand for vector(5,5)
local myVector = vector() --Shorthand for vector(0,0)

local myVector = vector.zero2 --Shorthand for vector(0,0)
local myVector = vector.right2 --Shorthand for vector(1,0)
local myVector = vector.up2 --Shorthand for vector(0,1)
local myVector = vector.one2 --Shorthand for vector(1,1)


--Some basic operators, fields, and functions exist for vectors:
myVector.length  --Length of the vector
myVector.sqrlength  --Squared length of the vector

local newVector = myNumber * myVector --Scalar multiplication
local newVector = myVector / myNumber --Scalar division
local newVector = myVectorA + myVectorB --Vector addition
local newVector = myVectorA - myVectorB --Vector subtraction
local newVector = myVectorA .. myVectorB --Dot product
local newVector = myVectorA ^ myVectorB --Cross product (3D only)
local newVector = myVectorA * myVectorB --Piecewise multiplication
local newVector = myVectorA / myVectorB --Piecewise division
local newVector = myVectorA % myVectorB --Project A onto B

local newVector = myVector:rotate(degrees) --2D rotation
local newVector = myVector:rotate(degrees, axis) --3D rotation
local newVector = myVector:normalize() --Set vector length to 1
local newVector = myVector:lookat(position) --Look towards vector
local newVector = myVector:planeproject(normal) --3D planar projection

local newVector = myVector:tov2() --Convert to 2D (3D and 4D only)
local newVector = myVector:tov3() --Convert to 3D (2D and 4D only)
local newVector = myVector:tov4() --Convert to 4D (2D and 3D only)



--You can construct 2x2, 3x3, or 4x4 square matrices, and use them for transformations by multiplying them with Vectors. 
local myMatrix = vector.mat2(m11, m12, m21, m22)

--Examples:
local myMatrix = vector.mat2(1,2,3,4) --Matrix   1  3
                                                 2  4
local myMatrix = vector.mat3(1,2,3,4,5,6,7,8,9) --Matrix   1  4  7
                                                           2  5  8
                                                           3  6  9
local myMatrix = vector.id2 --2D identity matrix (vector.mat2(1,0,0,1))
local myMatrix = vector.empty2 --Empty matrix (vector.mat2(0,0,0,0))
local myMatrix = vector.id3 --3D identity matrix
local myMatrix = vector.empty3 --Empty matrix


--Some basic operators, fields, and functions exist for matrices:
myMatrix.inverse --Inverse matrix (returns nil if matrix has no inverse)
myMatrix.det --Matrix determinant
myMatrix.trace --Matrix trace
myMatrix.transpose --Transposed matrix

local newMatrix = myNumber * myMatrix --Scalar multiplication
local newMatrix = myMatrix / myNumber --Scalar division
local newMatrix = myMatrixA + myMatrixB --Matrix addition
local newMatrix = myMatrixA - myMatrixB --Matrix subtraction
local newMatrix = myMatrixA * myMatrixB --Matrix multiplication
local newVector = myMatrix * myVector --Vector-Matrix multiplication


--You are also able to construct quaternions and use them for rotation of 3D Vectors
local myQuat = vector.quaternion(axis, degrees)

--Some basic operators, fields, and functions exist for quaternions:
myQuat.inverse --Quaternion inverse
myQuat.norm --Quaternion norm
myQuat.sqrnorm --Quaternion squared norm
myQuat.euler --Vector of euler angles
myQuat.normalised --Normalised quaternion

local newQuat = myNumber * myQuat --Scalar multiplication
local newQuat = myQuat / myNumber --Scalar division
local newQuat = myQuatA + myQuatB --Quaternion addition
local newQuat = myQuatA - myQuatB --Quaternion subtraction
local newQuat = myQuatA * myQuatB --Quaternion composition
local newVector = myQuatA * myVector --Quaternion application
local newQuat = myQuatA .. myQuatB --Quaternion dot product

local newQuat = myQuat:normalize() --Quaternion normalization
local newQuat = myQuat:lookTo(myVector, (upVector)) --Quaternion look at
local newQuat = myQuatA:rotateTo(myQuatB, (speed)) --Quaternion rotate
local newMatrix = myQuat:tomat() --Convert to 3x3 rotation matrix
local newMatrix = myQuat:tomat4() --Convert to 4x4 rotation matrix


--There are also a few general helper functions:
vector.randomDir2() --Random 2D vector with length 1
vector.randomOnCircle(radius) --Random 2D vector on the edge of a circle
vector.randomInCircle(radius) --Random 2D vector inside a circle

vector.randomDir3() --Random 3D vector with length 1
vector.randomOnSphere(radius) --Random 3D vector on the edge of a sphere
vector.randomInSphere(radius) --Random 3D vector inside a sphere
```

For more information on vectors, matrices, quaternions,
and the available functions for them, please refer to the library file
in scripts\\base\\vectr.lua directly.

### Achievements

The Achievement namespace allows you to access to
adjust achievements. There is not a lot that needs to be done with this
other than checking and setting conditions, so this guide will just
cover that.

To get a specific achievement, you can use the Achievement
constructor:

```lua
local myAchievement = Achievements(1)
```

From that, you can access the list of conditions, and either check
their values, or progress them. For example:

```lua
Misc.dialog(myAchievement:getCondition(1).value)
myAchievement:progressCondition(1)
Misc.dialog(myAchievement:getCondition(1).value)
```

This code will first display a dialog box showing the
current state of the first condition in your achievement. It then
progresses that condition, and shows the same dialog box again with the
updated condition. Here is the full documentation on these
functions:

```lua
Achievements.get() -- returns all achievements in the episode
Achievement:getCondition(id) -- returns the given condition object (use
                                Condition.value to access the state)
Achievement:progressCondition(id, delay?) -- progresses the condition.
                                             If delay is set to true,
                                             the achievement popup will
                                             not display until the next
                                            loading screen has finished 
Achievement:setCondition(id, delay?) -- sets the value of the condition. 
                                        If delay is set to true, the
                                        achievement popup will not 
                                        display until the next loading
                                        screen has finished
Achievement:resetCondition(id) -- resets the condition so it must be
                                  progressed again
```

### Checkpoints

The Checkpoint namespace allows you to create checkpoints from
Lua, which are a little more versatile than the editor-based ones. Note
that you must create
any checkpoints outside onStart, because they have to exist right at the
start of the level.

```lua
local myCheckpoint = Checkpoint{section = 0, x = -20000, y = -20000}
```

Checkpoint objects must contains a section, x, and y
field, which determine where the player will spawn. Other optional
fields are:

```lua
powerup --Powers up the player to this powerup (e.g. PLAYER_BIG for 
          default checkpoint behaviour)
sound --A sound file or ID to play when the checkpoint is collected 
        (e.g. 58 for default checkpoint behaviour)
actions --A function that will be run when the player spawns into this 
          checkpoint. The function will be run once for each player, and 
          will pass the player as an argument.
```

Once you have a checkpoint object, it won’t appear in
the level, but will exist in Lua and you can use Lua to collect the
checkpoint:

```lua
myCheckpoint:collect(player) --Collects the checkpoint, can optionally 
                               specify which player collected it.
myCheckpoint:reset() --Allows the checkpoint to be collected again.
```

Often, it is useful to be able to get the currently
active checkpoint. There are a few general functions that can help with
things like this:

```lua
Checkpoint.getActive() --Gets the currently active checkpoint (nil if no 
                        checkpoint is active)
Checkpoint.get() --Gets a list of all checkpoints
Checkpoint.get(id) --Gets the checkpoint with a specific ID
Checkpoint.reset() --Resets all checkpoints
```

There is also a global event that exists for
checkpoints, which is run whenever a checkpoint is collected, and allows
you to run code when the player hits a checkpoint:

```lua
function onCheckpoint(checkpoint, player)
    --your code here
end
```

### Explosion Class

Another new addition to the scripting functionality is
the Explosion class. You can use the Lua event onExplosion to detect
when an explosion happens, and do your own management of it:

```lua
function onExplosion(eventobj, explosion, player)
    if eventobj.cancelled then return end
    --your code here
end
```

The eventobj argument allows you to cancel the
explosion and prevent it from spawning. The explosion argument is the
explosion object itself, and the player argument is the player that
caused it.

The explosion object allows you to read and manipulate
certain things about the explosion:

```lua
x        --The x coordinate of the centre of the explosion
y        --The y coordinate of the centre of the explosion
radius   --The radius of the explosion
strong   --If true, will destroy grey brick blocks
friendly --If true, will not harm the player who spawned it
id       --The type of explosion this is
collider --A circle collider object representing the explosion hitbox
```

As well as onExplosion, there are a few basic functions
that can help work with explosions:

```lua
Explosion.spawn(x, y, id, player)   --Spawns an explosion
Explosion.get()   --Gets a list of explosion objects (use onExplosion 
                    for reacting to explosions)


Explosion.register(radius, effectID, soundEffect, (optional) strong, (optional) friendly)
--Registers a new explosion type (and returns its ID). Use this if you want to use custom explosions in your episode or level. You can then use this ID in Explosion.spawn. E.g.:

local myExplosion = Explosion.register(64, 13, “bang.ogg”)
Explosion.spawn(-20000, -20000, myExplosion, player)
```

By default, there are 5 different kinds of
explosions:

```lua
ID
 0    --Peach bomb
 1    --Unused (reserved ID)
 2    --SMB2 bomb
 3    --SMB3 bomb
 4    --TNT explosion
 5    --Nitro explosion
```

Using these IDs in Explosion.spawn will change the kind
of explosion that is spawned.

## Easy-Access Features

Easy-access libraries are fully usable with just a few
lines of code.

### Autoscrolling

```lua
local autoscroll = require("autoscroll")
```

Using the autoscroll library, you can create more
complicated autoscrolling scenarios than were previously possible in
SMBX. While SMBX 1.3 wouldn’t allow for autoscroll past the first
section the player spawns in, this library allows you to control when
autoscroll starts by yourself:

```lua
--As soon as the first section is loaded, begin an autoscroll to the right with a speed of 2.
function onLoadSection0()
    autoscroll.scrollRight(2)
end

--Further functions such as scrollUp, scrollLeft, scrollDown and scrollTo are also available.
--ScrollTo’s coordinate specifies the bottom left corner of the destination screen:
function onLoadSection1()
    --Scrolls to the specified X and Y coordinate with a speed of 1.5
    autoscroll.scrollTo(-170000, -185300, 1.5)
end
```

### Camera Zones

```lua
local camlock = require("camlock")
```

Using the camlock library, you can create camera zones. The
library is fairly rudimentary in its current stage and doesn’t respond
flawlessly to overlapping zones, but will allow you to control camera
movement more smoothly than you otherwise could with few lines of
code:

```lua
--The following function adds a camera zone with the specified bounds to the scene. Everything else is handled internally. lerpSpeed determines the speed at which the camera lerps into the zone once the player is inside of it. This argument is optional and should be between 0 and 1.
camlock.addZone(x, y, width, height, lerpSpeed)
```

### Clear pipes

Clear pipes automatically register a selection of NPCs
as candidates for passing through. You can modify these lists as
follows:

```lua
local clearpipe = require("blocks/ai/clearpipe")
--Register Goomba and unregister player fireball
clearpipe.registerNPC(1)
clearpipe.unregisterNPC(13)
```

### Lineguides

```lua
local lineguide = require("lineguide")
```

Lineguide is already used by several NPCs in basegame,
but it offers functions to register even more NPCs to its network of
rails.

```lua
--Registers the given IDs to lineguides.
lineguide.registerNpcs(ID or table of IDs)
```

### Orbits

```lua
local orbits = require("orbits")
```

Orbits allow you to create a set of NPCs moving in a
circle. The library has detailed documentation on how to use it in its
own orbits.lua file.

### Switch Colors

```lua
local switchcolors = require("switchcolors")
```

Switchcolors manages configurations and signals for
different colours of switches, including custom ones\!

```lua
--Registers a new switch colour. Returns an activator for it, as well as its numeric key for further checking.
local activator, myColor = switchcolors.registerColor(string name)

--Calling activator will toggle a switch
activator()

--You can use the switchcolors.onSwitch event to catch switch toggles
function switchcolors.onSwitch(color)
    if color == myColor then
        --do stuff specific to your switch colour
        --here you can use switchcolors.switch to switch blocks of 2 specific IDs
        switchcolors.switch(id1, id2)
    end
end

--The same procedure is also available for palace switches
local activator, myColor = switchcolors.registerPalace(string name)

activator()

function switchcolors.onPalaceSwitch(color)
    if color == myColor then
        --do stuff
    end
end
```

### Timer

```lua
local timer = require("timer")
```

Timer adds a timer to the level. Its addition to a
level also causes Green Berries to execute their effect upon being eaten
(adding 10 seconds to the timer).

```lua
--Activates the timer. Arguments default to 500 and false.
timer.activate(time, isFrames?)

--Sets the length of a second in frames.
timer.setSecondLength(frames)

timer.set(time, isFrames?)  --Sets the value of the timer.
timer.add(time, isFrames?)  --Adds time to the timer.

--Toggles timer active state if no newValue is specified. Else sets it.
timer.toggle(newValue)
```

## Advanced Libraries

Advanced libraries provide extra features but often
require more complicated setup and a deeper understanding of
lunalua.

### Actorclass - Easily Controllable Actors for Cutscenes 

Actorclass provides full-fledged controllable actors
for complex characters. It relies on animatx2.

The actorclass.lua file includes a detailed
documentation of the features of individual actors.

### Animatx2 - Extended spritesheet animations 

Animatx2 simplifies complex animation scripting by
taking care of the complicated math, leaving only the creation of
animation objects and their states as required things by the
user.

The animatx2.lua file includes a detailed documentation
of the library’s features.

### Click - Mouse Input

```lua
local click = require("click")
```

Click allows you to use a cursor in your level and use
clicks and mouse movement for user-defined interactions.

```lua
--Initialises all cursors. Each cursor definition is a table formatted as such: {table of images, xOffset, yOffset, framespeed}. You can leave the argument table empty in order to rely on default configurations.
click.loadCursor{table of cursor definitions}

--Stores current click state. Can be KEYS_PRESSED, KEYS_DOWN, KEYS_RELEASED or KEYS_UP
click.state

--Stores speed
click.speedX
click.speedY

--Sets which cursor to display
click.setCursorID(id)
```

For further functionality, refer to the click.lua lua
file.

### Routine - Coroutines

The Routine namespace serves as a wrapper for
coroutines in lunalua. It provides ways to run functions as coroutines.
It does not require loading to use. This works much the same way as the
older eventu.lua, and most of the behaviours are unchanged. However,
there are a few small differences and new things since the PAL version
(aside from not needing to load a library):

```lua
--Routine.run now lets you assign routines directly to variables.
local myRoutine = Routine.run(myFunc, args)

--Routine.wait allows you to provide a number of seconds to wait for.
Routine.wait(seconds, runWhilePaused?)

--Routine.skip allows you to skip to the next frame.
Routine.skip(runWhilePaused?)

--Routine.loop can be used inside routines to run code while waiting.
--This will wait for “frames”, and run myFunc every tick while waiting. Routine.loop(frames, myFunc, runWhilePaused?)

--Functions like abort can be called directly from the routine object.
myRoutine:abort()

--The pause and resume functions can pause timed waits, and will always
--prevent a routine from continuing, even if the wait condition is met.
myRoutine:pause()
myRoutine:resume()

--A few fields can be used to check the state of the routine.
---isValid: false if the routine has ended
---paused: true if the routine has been paused with myRoutine:pause()
---yielded: true if the routine was yielded with Routine.yield()
---waiting: true if the routine is currently waiting for anything
myRoutine.isValid
myRoutine.paused
myRoutine.yielded
myRoutine.waiting

--Routine.yield gives you manual control over when to resume a routine.
Routine.yield()

--myRoutine:continue resumes a yielded routine.
myRoutine:continue()
```

### Handycam - Advanced Camera Control

```lua
local handycam = require("handycam")
```

Handycam makes it easy to perform complex operations on
SMBX’s cameras.

```lua
--Cameras are accessed through handycam[idx]
handycam[1]

--Available fields:
x,y            
xOffset, yOffset
width, height
rotation     
zoom
targets     --List of targets the camera is focusing on

--Available methods
--For a detailed explanation of their arguments and function, please refer to the handycam.lua library file
handycam[idx]:transition     --Immediately performs a transition
handycam[idx]:queue          --Adds a transition to the end of the queue
handycam[idx]:unqueue        --Removes a transition from the queue
handycam[idx]:clearQueue     --Clears the queue
handycam[idx]:release        --Returns camera control to SMBX
handycam[idx]:worldToScreen  --Converts coord from world to camera space
handycam[idx]:screenToWorld  --Vice-versa
Handycam[idx]:reset          --Resets to default behaviour
Handycam[idx]:finish         --Finishes all transitions immediately
Handycam[idx]:abort          --Aborts all transitions immediately
Handycam[idx]:skip           --Finishes active transitions immediately
```

### Particles - Particle Effects and Ribbon Trails

Particles allows you to create and attach particle
emitters and ribbons to various objects. Its usage hasn’t changed much
since Beta 3, and the documentation below is still mostly
accurate:

[wohlsoft.ru/pgewiki/Particles.lua](http://wohlsoft.ru/pgewiki/Particles.lua)

However, the overload for Emitter:draw has been
extended, with all arguments still being optional:

```lua
Emitter:draw(priority, disableCulling?, renderTarget, sceneCoords?, color, timeScale, updateWhilePaused?)
```

### Darkness - Darkness and Lighting

While this library is accessible directly from the
editor, you can also opt to use it yourself in order to create more
complex effects. It does not require loading, as it is automatically
loaded.

The darkness library allows you to create dark
sections. You may have noticed various light-related configuration flags
in the sections of this handbook related to BGO and NPC configuration.
Once a darkness zone exists in the level, those take effect.

To add a darkness zone, you do the following:

```lua
local myDarknessField = Darkness.create{args}
--Args allows the following named arguments:
--falloff           - Determines how light intensity in this field should propagate. Defaults to  
                          darkness.falloff.DEFAULT. Paths to shaders can also be supplied.
--shadows           - Determines how shadows should be rendered in this field. Defaults to 
                               darkness.shadow.DEFAULT. Paths to shaders can also be supplied.
--maxLights         - Maximum number of lights that can be rendered at any one time. Defaults to 60.
--uniforms          - Table of extra uniforms to supply to the shader (can be used to tweak behaviour).
--priorityType      - Determines how lights should be selected if there are too many to render. Defaults to
                                 darkness.priority.DISTANCE.
--bounds            - A rect specifying the boundaries of this field. If left nil, will apply to the entire scene.
--boundBlendLength  - Size of fadeout on the boundaries of this field, if they are used. Defaults to 64.
--section           - Which section this field should apply to. -1 means all sections. Defaults to -1.
--sections  `       - Takes precedence over "section". Allows multiple sections to be specified in a table.
--ambient           - The ambient light colour. Defaults to 0x0D0D19.
--priority          - The render priority of this field. Defaults to 0.
```

In addition to relying on the lights attached to the
player and certain objects by default, you can create your own lights
manually:

```lua
--Creates a light
local myLight = Darkness.light(x,y,radius,brightness,color)

--Once you have a light, you have to activate it
Darkness.addLight(myLight)
Darkness.removeLight(myLight) --Removal also possible

--If you want a light to only affect specific darkness fields, you can use the following function
myDarknessField:addLight(myLight)
myDarknessField:removeLight(myLight) --Removal also possible
```

For further functions, please refer to the darkness.lua
file directly.

### Textplus - Advanced Dialogue Framework

```lua
local textplus = require("textplus")
```

Textplus is a more robust rework of the old textblox
library. It’s still WIP, and as such doesn’t include all of the old
library’s features by default. Features such as word bubbles have to
manually be built around at this stage, but textplus’ internal functions
provide various features which neither the vanilla text rendering nor
textblox are capable of.

The textplus.lua library contains documentation on its
functions.

## Shader Programming

With this new release we are introducing GLSL shader
programming to lunalua. Shaders are programmed using GLSL Version 1.2,
with vertex and fragment parts of a shader split between .vert and .frag
files. In the data\\\_templates directory you are able to find
standard.vert and standard.frag files, which serve as cloneable
templates for your own shader files.

You can then use your own shaders in your lua code like
so:

```lua
--Initialize the shader
local myShader = Shader()

--Compile the shader
function onStart()
    --Vert and Frag default to their standard implementations, so if you use a standard shader, you can leave that argument as “nil”.
    myShader:compileFromFile(vert, frag)
end

--Shaders can be used with the glDraw drawing function.
function onDraw()
    Graphics.glDraw{
        ...
        shader=myShader,
        --texture is parsed as iChannel0,
        uniforms = {
            --Define uniforms that are used by the shader
        },
        attributes = {
            --Define attributes that are used by the shader (a flattened 
              array of data with one object per vertex)
        }
    }
end
```

[glDraw
Documentation](https://wohlsoft.ru/pgewiki/Graphics.glDraw)

[The GLSL Documentation contains example shaders to
learn
from](http://www.lighthouse3d.com/tutorials/glsl-12-tutorial/)
