# Editor Configurations

When working with custom range items or when adding extra settings to previously existing objects (blocks, BGOs, NPCs), you have to create an ini file where these settings are defined for the editor to read.

[Specification of Block ini files](https://wohlsoft.ru/pgewiki/Block_Entry_INI_config_(Config_pack))

[Specification of BGO ini files](https://wohlsoft.ru/pgewiki/Background_Object_Entry_INI_config_(Config_pack))

[Specification of NPC ini files](https://wohlsoft.ru/pgewiki/NPC_Entry_INI_config_(Config_pack))

The pre-packaged ini files from the _templates directory can help in providing a head start in setting up your ini file.

<Note type="tip">Not all settings on these pages are relevant to SMBX2, nor does every setting need to be included in every ini.</Note>

<Note type="tip">Aside from the extra-settings field, fields in the ini only affect an object's in-editor appearance.</Note>