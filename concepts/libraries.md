# Libraries

A library, or module, is a lua file that can be loaded by another lua file to expose additional functionality. Libraries are used to make code easier to repurpose and share with others.

## Loading libraries

To load a library, use the require-Function with the name of the library. The name is automatically resolved in the level folder, episode folder, data/scripts directory and data/scripts/base directory. To load a file from a subfolder, the relative path to one of these must be used.

```lua
-- When loading a library, do not add the '.lua' file extension.
local textplus = require("textplus")
```

## Creating Libraries

Libraries are fundamentally tables with extra functionality added by the user. Take this library for example, which outlines a barebones structure of a library:

```lua
-- First, declare the table you want to return.
local libraryTable = {}

-- Local values cannot be accessed outside of the scope of the library.
local myNumber = 2
-- To export a variable as part of the library, make it a part of the library table.
libraryTable.myBool = false

-- The same principle applies to functions.
local function add(a, b)
    return a + b
end

function libraryTable.subtract(a, b)
    return a - b
end

-- The onInitAPI function can be used if you want to use lunalua events in your library
function libaryTable.onInitAPI()
    -- This causes the onStart event to be called on this library
    registerEvent(libraryTable, "onStart")
end

-- The onStart event also needs to be part of the library table for this to work
function libraryTable.onStart()
    Misc.dialog("onStart says", "Hello!")
end

-- Finally, return the table
return libraryTable
```

Using this library, called, say, "testLibrary.lua", would allow you to do the following, then:

```lua
local testLibrary = require("testLibrary")

-- This will print nil, because the myNumber variable only exists in the library (see above)
Misc.dialog("myNumber is", myNumber)

-- This will print false. The boolean is accessible.
Misc.dialog("myBool is", testLibrary.myBool)

-- Exposed variables can be set externally.
testLibrary.myBool = true
Misc.dialog(testLibrary.myBool)

-- The same principle applies to functions.
Misc.dialog("Subtracting 3 from 5 gives", testLibrary.subtract(5, 3))

-- After all these dialogs appeared, the library's onStart will also execute.
```

